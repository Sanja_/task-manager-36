package ru.karamyshev.taskmanager.constant;

import org.springframework.stereotype.Component;

@Component
public interface MsgCommandConst {

    String COMMAND_ABSENT = "\n Error! Command absent.";

}
