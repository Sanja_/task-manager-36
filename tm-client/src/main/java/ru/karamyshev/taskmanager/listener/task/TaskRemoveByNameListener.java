package ru.karamyshev.taskmanager.listener.task;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.*;
import ru.karamyshev.taskmanager.event.ConsoleEvent;
import ru.karamyshev.taskmanager.listener.AbstractListener;
import ru.karamyshev.taskmanager.service.SessionService;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.lang.Exception;
import java.util.List;

@Component
public class TaskRemoveByNameListener extends AbstractListener {

    @NotNull
    @Autowired
    private SessionService sessionService;

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-tskrmvnm";
    }

    @NotNull
    @Override
    public String command() {
        return "task-remove-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by name.";
    }

    @Override
    @EventListener(condition = "@taskRemoveByNameListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        final SessionDTO session = sessionService.getSession();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK NAME FOR DELETION:");
        final String name = TerminalUtil.nextLine();
        taskEndpoint.removeTaskOneByName(session, name);
        System.out.println("[OK]");
    }

}
