package ru.karamyshev.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;

@Component
public interface ISessionService {

    @Nullable
    void setSession(SessionDTO session);

    void clearSession();

    @Nullable
    SessionDTO getSession();

}
