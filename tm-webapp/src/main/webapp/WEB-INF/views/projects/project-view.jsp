<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../resources/_header.jsp"/>

    <body>
        <h3>Project View</h3>
        <table>
            <tr>
                <td>Name :</td>
                <td>"${project.name}"</td>
            </tr>

            <tr>
                <td>Description :</td>
                <td>"${project.description}"</td>
            </tr>
        </table>

        <table width="100%" cellpadding="10" border="2" style="border-collapse: collapse;">
                <tr>
                    <th width="200">ID</th>
                    <th width="200">NAME</th>
                    <th width="200">DESCRIPTION</th>
                </tr>

                <c:forEach var="task" items="${tasks}">
                    <td><c:out value="${task.id}"/></td>
                    <td><c:out value="${task.name}"/></td>
                    <td><c:out value="${task.description}"/></td>
                   </tr>
                </c:forEach>
            </table>
    </body

<jsp:include page="../resources/_footer.jsp"/>