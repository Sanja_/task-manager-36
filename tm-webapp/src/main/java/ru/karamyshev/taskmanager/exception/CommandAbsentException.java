package ru.karamyshev.taskmanager.exception;

import org.springframework.stereotype.Component;

@Component
public class CommandAbsentException extends RuntimeException{

    public CommandAbsentException() {

        super("Error! Index is  incorrect...");
    }

}
