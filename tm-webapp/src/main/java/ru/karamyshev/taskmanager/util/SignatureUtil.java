package ru.karamyshev.taskmanager.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.Nullable;

public final class SignatureUtil {

    @Nullable
    public static String sign(final Object value, String salt, Integer cycle) {
        final ObjectMapper objectMapper = new ObjectMapper();
        try {
            final String json = objectMapper.writeValueAsString(value);
            return sign(json, salt, cycle);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    @Nullable
    public static String sign(String value, String salt, Integer cycle) {
        if (value == null || salt == null) return null;
        String result = value;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.md5(salt + result + salt);
        }
        return result;
    }

}
