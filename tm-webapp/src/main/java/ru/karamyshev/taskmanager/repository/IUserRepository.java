package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.User;


public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@NotNull String login);

    void deleteByLogin(@NotNull String login);

}
