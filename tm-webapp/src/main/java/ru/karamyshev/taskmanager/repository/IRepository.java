package ru.karamyshev.taskmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.karamyshev.taskmanager.entity.AbstractEntity;

public interface IRepository<E extends AbstractEntity> extends JpaRepository<E, String> {

}
