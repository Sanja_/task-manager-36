package ru.karamyshev.taskmanager.controller;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import ru.karamyshev.taskmanager.entity.Task;
import ru.karamyshev.taskmanager.repository.ITaskRepository;

@Controller
@NoArgsConstructor
public class TaskController {

    @Autowired
    private ITaskRepository taskRepository;

    @GetMapping("/tasks")
    public ModelAndView index() {
        return new ModelAndView("/tasks/tasks-list",
                "tasks", taskRepository.findAll());
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        taskRepository.deleteById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/create")
    public String create(@ModelAttribute("task") Task task) {
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/create")
    public ModelAndView create(Model model) {
        final Task task = new Task();
        return new ModelAndView("/tasks/task-create", "task" , task );
    }


    @PostMapping("/task/edit/{id}")
    public String edit(Task task) {
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final Task task = taskRepository.findById(id).orElse(null);
        return new ModelAndView("/tasks/task-edit", "task", task);
    }

    @GetMapping("/task/view/{id}")
    public ModelAndView view(@PathVariable("id") String id) {
        final Task task = taskRepository.findById(id).orElse(null);
        return new ModelAndView("/tasks/task-view", "task", task);
    }

}
