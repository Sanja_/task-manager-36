package ru.karamyshev.taskmanager.controller;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.entity.Task;
import ru.karamyshev.taskmanager.repository.IProjectRepository;
import ru.karamyshev.taskmanager.repository.ITaskRepository;

import java.util.List;


@Controller
@NoArgsConstructor
public class ProjectController {

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private ITaskService taskService;

    @GetMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("/projects/projects-list",
                "projects", projectRepository.findAll());
    }

    @GetMapping("/project/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        projectRepository.deleteById(id);
        return "redirect:/projects";
    }

    @PostMapping("/project/create")
    public String create(@ModelAttribute("project") Project project) {
        projectRepository.save(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/create")
    public ModelAndView create(Model model) {
        final Project project = new Project();
        return new ModelAndView("/projects/project-create", "project", project);
    }

    @PostMapping("/project/edit/{id}")
    public String edit(Project project) {
        projectRepository.save(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final Project project = projectRepository.findById(id).orElse(null);
        return new ModelAndView("/projects/project-edit", "project", project);
    }

    @GetMapping("/project/view/{id}")
    public ModelAndView view(@PathVariable("id") String id) {
        @Nullable final ModelAndView model = new ModelAndView("/projects/project-view");
        @Nullable final Project project = projectRepository.findById(id).orElse(null);
        @Nullable final List<Task> tasks = taskService.findAllByProjectId("test userId", id);
        model.addObject("project", project);
        model.addObject("tasks", tasks);
        return model;
    }

}
