package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @Nullable List<Project> findAllByUserId(@NotNull String userId);

    @Nullable Project findByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Nullable Project deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Nullable Project findByUserIdAndName(@NotNull String userId, @NotNull String name);

    @Nullable Project deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    @Nullable Project deleteAllByUserId(@NotNull String userId);

}
