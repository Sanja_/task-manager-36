package ru.karamyshev.taskmanager.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.karamyshev.taskmanager.api.endpoint.IUserEndpoint;
import ru.karamyshev.taskmanager.api.service.ISessionService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.dto.SessionDTO;
import ru.karamyshev.taskmanager.dto.UserDTO;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
@NoArgsConstructor
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Nullable
    @Autowired
    private IUserService userService;

    @Nullable
    @Autowired
    private ISessionService sessionService;

    @Override
    @WebMethod
    public void createUser(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password
    ) throws Exception {
        userService.create(login, password);
    }

    @Override
    @WebMethod
    public void createUserEmail(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "email", partName = "email") @Nullable String email
    ) throws Exception {
        userService.create(login, password, email);
    }

    @Nullable
    @Override
    @WebMethod
    public void createUserRole(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "role", partName = "role") @Nullable Role role
    ) throws Exception {
        userService.create(login, password, role);
    }

    @Nullable
    @Override
    @WebMethod
    public User findUserById(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session);
        return userService.findById(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public User findUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        sessionService.validate(session);
        return userService.findByLogin(login);
    }

    @Override
    @WebMethod
    public void removeUser(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "user", partName = "user") @Nullable User user
    ) throws Exception {
        sessionService.validate(session);
        userService.removeUser(user);
    }

    @Override
    @WebMethod
    public void removeUserById(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session);
        userService.removeById(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public void removeByLogin(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        sessionService.validate(session);
        userService.removeByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        sessionService.validate(session);
        @Nullable final User currentUser = userService.findById(session.getUserId());
        userService.removeUserByLogin(currentUser.getLogin(), login);
    }

    @Nullable
    @Override
    @WebMethod
    public List<UserDTO> getUserList(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session);
        return userService.findAll();
    }

    @Override
    @WebMethod
    public void renamePassword(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword
    ) throws Exception {
        sessionService.validate(session);
        userService.renamePassword(session.getUserId(), newPassword);
    }

    @Override
    @WebMethod
    public void loadUser(
            @WebParam(name = "users", partName = "users") @Nullable List<User> users,
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        sessionService.validate(session);
        //  userService.load(users);
    }

}